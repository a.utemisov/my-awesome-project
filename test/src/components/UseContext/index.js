import styles from './index.module.css'
import {data} from './data'

import React, { useContext, useState } from 'react'

const PersonContext = React.createContext();

const ContextAPI = () => {
  const [people, setPeople] = useState(data);
  
  const logToConsole = (name) => {
    console.log(name)
  }

  return(
    <PersonContext.Provider value={{people, logToConsole}}>
      <div className={styles.wrapper}>
        <div>This is context</div>
        <List/>
      </div>
    </PersonContext.Provider>
  )
}

const List = () => {
  const peopleData = useContext(PersonContext)
  return(
    <>
      {peopleData.people.map((person) => {
        return <ListItem key={person.id} {...person}></ListItem>
      }
      )}
    </>
  )
}
    

const ListItem = ({name}) => {
  const func = useContext(PersonContext)
  return (
    <div>
      {`${name}_________`}
      <button onClick={() => func.logToConsole(name)}>log</button>
    </div>
  )
}
export default ContextAPI;