import { useEffect } from "react"

const Modal = ({modalContent, closeModal}) => {
  useEffect(()=>{
    setTimeout(()=>{closeModal()},1000)
  })
  return(
    <>
      {modalContent}
    </>
  )
}

export default Modal