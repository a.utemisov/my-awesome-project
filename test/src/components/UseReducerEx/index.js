import { useReducer, useState } from "react"
import Modal from "./modal"
import { reducer } from "./reducer"


const defaultState = {
  people: [],
  isModalOpen: false,
  modalContent: 'Hello World!'
}

const UseReducerEx = () => {
  const [name, setName] = useState('')
  const [state, dispatch] = useReducer(reducer, defaultState)
  
  
  const submitHandler = (e) => {
    e.preventDefault()
    if(name){
      const newItem = {id: new Date().getTime().toString(), name}
      dispatch({type: 'ADD_ITEM', payload: newItem})
      setName('')
    }else{
      dispatch({type: 'NO_VALUE'})
    }
  }

  const closeModal = () => {
    dispatch({type: 'CLOSE_MODAL'})
  }

  return (
  <>
    {state.isModalOpen && <Modal closeModal={closeModal} modalContent={state.modalContent}/>}
    <form onSubmit={submitHandler}>
      <input type='text' value={name} onChange={(e) => setName(e.target.value)}/>
      <button type='submit'>asd</button>
    </form>
    {state.people.map((person) => {
      return(
        <div key = {person.id}>
          <h4>{person.name}</h4>
          <button onClick={() => {
            dispatch({type: 'REMOVE', payload: person.id})
          }}>delete</button>
        </div>
      )
    }
    )}
  </>

  )
}
export default UseReducerEx