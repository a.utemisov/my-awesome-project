export const reducer = (state, action) =>{
  
  switch(action.type){
    case 'ADD_ITEM' : {
      const newPeople = [...state.people, action.payload] 
      return {
        ...state,
        people: newPeople,
        isModalOpen: true,
        modalContent: 'Something was added'
      }
    }
    case 'NO_VALUE' :{
      return {
          ...state,
          isModalOpen: true,
          modalContent: 'Plz enter something'
      }
    }
    case 'CLOSE_MODAL' :{
      return {
        ...state,
        isModalOpen: false,
      }
    }
    case 'REMOVE' : {
      const newPeople = state.people.filter((person) => person.id !== action.payload)
      return {
        ...state,
        people: newPeople,
      }
    }
    default : 
      throw new Error('Powel Nahui');
  }
}