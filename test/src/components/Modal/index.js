import { useGlobalContext } from "../GlobalContext"
import styles from './Modal.module.css'
const Modal = () => {

  const {isModalOpen, closeModal} = useGlobalContext()

  return (
    <div className={`${isModalOpen ? styles.modal : styles.hide_modal}`}>
      <span>This is Modal</span>
      <button onClick={closeModal}>Close</button>
    </div>
  )
}

export default Modal