import links from '../../data/links.json'
import styles from './NavBar.module.css'
import {useEffect, useRef, useState} from 'react'
import { useGlobalContext } from '../GlobalContext'

const NavBar = () => {
  const {isModalOpen, openModal} = useGlobalContext()

  const [toogle, setToogle] = useState(true)

  const linksContainerRef = useRef(null);
  const linksRef = useRef(null);

  const _links = links.links;
  useEffect(()=>{
    const linksHeight = linksRef.current.getBoundingClientRect().height;
    if(toogle){
      linksContainerRef.current.style.height = `${linksHeight}px`
      // console.log(linksHeight)
    }else{
      linksContainerRef.current.style.height = `73.6px`
      // console.log(linksHeight)
    }
  },[toogle])

  return (
  <>
    <div className={styles.navBar_container} ref={linksContainerRef}>
      <span className={styles.logo}>Keks</span>
      <ul className={styles.link_list} ref={linksRef}>
        {_links.map((link) => {
          return <li className={styles.link} key={link.id}><a href='http://localhost:3000/'>{link.name}</a></li>
          })
        }
      </ul>
      <ul className={styles.social_media}>
        <li>1</li>
        <li>2</li>
        <li>3</li>
      </ul>
      <button className={styles.toogle} onClick={()=> setToogle(!toogle)}>keks</button>
      <button onClick={openModal}>Show</button>
    </div>
  </>
  )
}

export default NavBar