import NavBar from "./components/NavBar";
import UseReducerEx from "./components/UseReducerEx";
import ContextAPI from "./components/UseContext";
import Modal from "./components/Modal";
import { AppProvider } from "./components/GlobalContext";


function App() {
  return (
    <AppProvider>
      <div className="App">
        <NavBar/>
        <UseReducerEx/>
        <ContextAPI/>
        <Modal/>
      </div>
    </AppProvider>
  );
}

export default App;
