import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { useState } from 'react';

import { About } from './pages/About';
import { Home } from './pages/Home';
import { Products } from './pages/Products';
import { SharedLayout } from './SharedLayout'
import { Error } from './pages/Error'
import { SingleProduct } from './pages/SingleProduct';
import { Login } from './pages/Login'
import { Dashboard } from './pages/Dashboard';
import { ProtectedRoute } from './pages/ProtectedRoute';


import styles from './styles/pages.module.css'
import { SharedProductLayout } from './SharedProductLayout';
function App() {

  const [user, setUser] = useState(null);

  return (
    <BrowserRouter>
      <div className={styles.page_container}> 
        <Routes>
          <Route path='/' element={<SharedLayout/>}>
            <Route index element={<Home/>}/>
            <Route path='about' element={<About/>}/>
            
            <Route path='products' element={<SharedProductLayout/>}>
              <Route index element={<Products/>}/>
              <Route path=':productId' element={<SingleProduct/>}/>
            </Route>

            <Route path='login' element={<Login setUser = {setUser}/>}/>
            <Route path='dashboard' 
              element={
              <ProtectedRoute user={user}>
                <Dashboard user = {user}/>
              </ProtectedRoute>
              }
            />
            <Route path='*' element={<Error/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
