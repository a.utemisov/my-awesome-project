import { NavLink } from "react-router-dom"
import styles from '../styles/pages.module.css'
export const Navbar = () => {
  return (
    <div className={styles.navbar}>
      
      <NavLink to='/'
        className={({isActive}) => isActive ? styles.active : styles.notActive 
        }>
        Home 
      </NavLink>
      <NavLink to='/about' 
        className={({isActive}) => {
          return isActive ? styles.active : styles.notActive 
        }}>
        About
      </NavLink>
      <NavLink to='/products'
        style={({isActive}) => {
          return { color: isActive ? 'red' : 'blue'}
        }}>
        Products 
      </NavLink>
      <NavLink to='/login' 
        className={({isActive}) => {
          return isActive ? styles.active : styles.notActive 
        }}>
        Login
      </NavLink>
    </div>
  )
}