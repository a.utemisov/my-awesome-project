import { Link } from "react-router-dom"
import links from '../data/links.json'

import styles from '../styles/pages.module.css'

export const Products = () => {
  const products = links.links
  return (
    <>
      <h2>Products Page</h2>
      <div className={styles.products}>
        {products.map((product) => {
          return <article key={product.id}>
            <h3>{product.name}</h3>
            <Link to={`/products/${product.id}`}>More info</Link>
          </article>
        })}
      </div>
    </>
  )
}