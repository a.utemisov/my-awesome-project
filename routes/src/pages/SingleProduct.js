import { Link, useParams } from "react-router-dom"
import links from '../data/links.json'

export const SingleProduct = () => {
  const {productId} = useParams();
  const products = links.links
  const product = products.find((product) => product.id.toString() === productId )
  console.log(product)
  const {name} = product
  return (
    <>
      <h2>SingleProducts Page {productId} "{name}"</h2>
      <Link to='/products'>Back to Products</Link>
    </>
  )
}