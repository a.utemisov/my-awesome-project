import { useState } from "react"
import { useNavigate } from "react-router-dom"

import styles from '../styles/pages.module.css'

export const Login = ({setUser}) => {

  const [name, setName] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const handleSubmit = async (e) => {
    e.preventDefault()
    if (!name || !password)
      return
    console.log(name, password)
    setUser({name: name, password: password})
    navigate('/dashboard')
  }

  return (
    <section className={styles.login_page}>
      <form onSubmit={handleSubmit} className={styles.form}>
        <h2>
          This is Login
        </h2>
        <div className={styles.username}>
          <label className={styles.form_label} htmlFor="username">Login</label>
          <input className={styles.form_input} type='text' id='username' value={name} onChange={(e) => setName(e.target.value)}/>
        </div>
        <div className={styles.password}>
          <label className={styles.form_label} htmlFor="password">Password</label>
          <input className={styles.form_input} type='password' id='password' value={password } onChange={(e) => setPassword(e.target.value)}/>
        </div>
        <button type='submit'>Log in</button>
      </form>
    </section>
  )
}