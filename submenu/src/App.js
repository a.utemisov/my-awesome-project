import { AppProvider } from "./GlobalContext";
import styles from './App.module.css'
import { Navbar } from "./components/Navbar/Navbar";
import { BrowserRouter, Routes, Route} from 'react-router-dom'
function App() {
  
  return (
    <AppProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element ={
            <div className={styles.App}>
              <div className={styles.navbar}>
                <Navbar/>
              </div>
            </div>
            }
          />
          <Route path="/kek" element={
            <div>kek</div>
          }/>
      </Routes>
      </BrowserRouter>
    </AppProvider>
  );
}

export default App;
