const data = [
  { 
    id: 1,
    name: 'first',
    subs: [
      {
        label: 'one',
        url: 'http://localhost:3000/'
      },
      {
        label: 'two',
        url: 'http://localhost:3000/'
      },
      {
        label: 'three',
        url: 'http://localhost:3000/'
      },
      {
        label: 'four',
        url: 'http://localhost:3000/'
      }
    ]
  },
  {
    id: 2,
    name: 'second',
    subs: [
      {
        label: '1',
        url: 'http://localhost:3000/'
      },
      {
        label: '2',
        url: 'http://localhost:3000/'
      },
      {
        label: '3',
        url: 'http://localhost:3000/'
      },
    ]
  },
  {
    id: 3,
    name: 'third',
    subs: [
      {
        label: 'raz',
        url: 'http://localhost:3000/'
      },
      {
        label: 'dwa',
        url: 'http://localhost:3000/'
      },
      {
        label: 'tree',
        url: 'http://localhost:3000/'
      },
      {
        label: 'chetyre',
        url: 'http://localhost:3000/'
      }
    ]
  },
]
export default data