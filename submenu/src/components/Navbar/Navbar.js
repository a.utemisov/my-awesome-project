import {Menu} from '../Menu/Menu'
// import {Submenu}from '../Submenu/Submenu'

import styles from './Navbar.module.css'
import data from '../../data'

import { useGlobalContext } from '../../GlobalContext'
import { useState } from 'react'

export function Navbar() {
  let left
  const contextData = useGlobalContext()

  const [tabInfo, setTabInfo] = useState();

  const showTabInfo = (e, name, subs) => {
    // let right = (e.target.getBoundingClientRect().right) / 2;
    left = (e.target.getBoundingClientRect().left);
    setTabInfo({coordinates: left, name: name, subs: subs});
    contextData.openMenu()
  }

  const closeHandler = (e) => {
    // if(!e.target.classList.contains('navbar_list_item'))
    // console.log(e.target)
    // // contextData.closeMenu()
  }

  return (
    <>
      <div className={styles.navbar_container} onMouseOver={closeHandler}>
        <span className={styles.logo}>LOGO</span>
        <ul className={styles.navbar_list} >
          {data.map(tab => <li 
            className={styles.navbar_list_item} 
            onMouseOver = {(e) => showTabInfo(e, tab.name, tab.subs)}
            key={tab.id}
            >
              {tab.name}
            </li>
            )
          }
        </ul>
        <button className={styles.toogle_sidebar}>Side Bar</button>
        <button className={styles.toogle_singIn}>Sing In</button>
      </div>
      {contextData.isMenuOpen && <Menu {...tabInfo} />}
    </>
  )
}