import { useEffect, useRef, useState } from 'react'
import styles from './Menu.module.css'
import {useGlobalContext} from '../../GlobalContext'

export function Menu({coordinates, name, subs}){
  const [columns, setColumns] = useState('col-2')

  const container = useRef(null)

  const {closeMenu} = useGlobalContext();

  useEffect(() => {
    setColumns('col-2')
    container.current.style.left = `${coordinates}px`
    if(subs.length % 3 === 0) {
      setColumns('col-3')
    }
  }, [coordinates])


  return(
    <div className={styles.menu_container} ref={container} onMouseLeave= {closeMenu}>
      <h4 className={styles.list_name}>{name}</h4>
      <ul className={`${styles.menu_list} ${columns}`}>
        {subs.map((link,index) =>
          <li key={index} className={styles.menu_list_item}>
            <a  href={link.url}>{link.label}</a>
          </li>
          )}
      </ul>
    </div>
  )
}